#!/usr/bin/env bash

# -------------- #
#   Arg parser   #
# -------------- #

force_install=0
override_vars=0

while [ $# -gt 0 ]; do
    case "$1" in
        -f|--force-install)
            force_install=1
            shift 1
            ;;
        -o|--override-vars)
            override_vars=1
            shift 1
            ;;
        *)
            break
            ;;
    esac
done

# ----------- #
#   Helpers   #
# ----------- #

# Pretty output
t_red=$(tput setaf 1)
t_green=$(tput setaf 2)
t_yellow=$(tput setaf 3)
t_cyan=$(tput setaf 6)
t_reset=$(tput sgr0)
t_bold=$(tput bold)

# Show section title
T() {
    echo
    echo "${t_bold}+${t_reset} ${t_cyan}$@"
    echo "  $(echo $@ | sed 's/./-/g')${t_reset}"
    echo
}

# ----------------------------------- #
T   Environment checks "($(uname))"   #
# ----------------------------------- #

# Testing environment for installed tools and software
env_check_failed=0
_check() {
    where=$1
    rtn_code=$2
    name=$3
    howto_install=$4
    if [ "$rtn_code" -eq 0 ]; then
        echo "  ${t_green}Found ${t_bold}${name}${t_reset} ${t_green}${where}${t_reset}"
    else
        env_check_failed=1
        echo "  ${t_red}Missing ${t_bold}${name}${t_reset} ${t_red}${where}${t_reset}"
        if [[ "$howto_install" != "" ]]; then
            echo "   ${t_yellow}*-> Fix \$${t_reset} ${howto_install}"
        fi
    fi
}

check_bin() {
    bin_name=$1
    howto_install=$2
    which $bin_name 2>&1 >/dev/null
    rtn_code=$?
    _check "in PATH" $rtn_code $bin_name "$howto_install"
}

check_fs() {
    file_name=$1
    howto_install=$2
    [ -f $file_name ] || [ -d $file_name ]
    rtn_code=$?
    _check "on filesystem" $rtn_code $(basename $file_name) "$howto_install"
}

info_uname() {
    linux_info=$1
    unix_info=$2
    if [[ $(uname) == "Darwin" ]]; then
        echo $unix_info
    else
        echo $linux_info
    fi
}

if [[ $(uname) == "Darwin" ]]; then
    PATH="/etc:/usr/local/bin:$PATH" # Locally setting proper path for checks on OSX

    check_bin brew \
        '/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"'
    check_bin ggrep \
        'brew install grep'
    check_bin gls \
        'brew install coreutils'
else
    check_bin grep
fi

check_bin vim
check_bin git
check_bin zsh
check_bin most
check_bin tree
check_bin wget
check_bin jq
check_bin python3 "$(info_uname \
    'apt-get install python3' \
    'brew install python' \
)"
# TODO: Make golang and protoc optional, but still checked for existence (yellow if not present)
#check_bin go "$(info_uname \
#    'apt-get install golang' \
#    'brew install go' \
#)"
#check_bin protoc "$(info_uname \
#    'open https://github.com/protocolbuffers/protobuf/blob/master/src/README.md' \
#    'brew install protobuf@3.7' \
#)"

check_fs ~/.oh-my-zsh \
    'sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"'
check_fs ~/.fzf.zsh "$(info_uname \
    'git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf && ~/.fzf/install' \
    'brew install fzf && $(brew --prefix)/opt/fzf/install' \
)"
#check_fs ~/.z.sh \
#    'wget -O ~/.z.sh https://raw.githubusercontent.com/rupa/z/master/z.sh'
check_fs ~/.kube-ps1.sh \
    'wget -O ~/.kube-ps1.sh https://raw.githubusercontent.com/jonmosco/kube-ps1/master/kube-ps1.sh'
check_fs ~/.oh-my-zsh/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh \
    'brew install zsh-syntax-highlighting && ln -s $(brew --prefix)/share/zsh-syntax-highlighting ~/.oh-my-zsh/zsh-syntax-highlighting'
check_fs ~/.vim/pack/plugins/start/vim-toml \
    'mkdir -p ~/.vim/pack/plugins/start && cd ~/.vim/pack/plugins/start && git clone https://github.com/cespare/vim-toml.git'

if [ $env_check_failed -eq 1 ] && [ $force_install -eq 0 ]; then
    echo
    echo "  ${t_red}Environment checks failed: install missing dependencies or re-run with ${t_bold}--force-install${t_reset}"
    echo
    exit 1
fi

# ----------------------------- #
T   Setting zsh as user shell   #
# ----------------------------- #

# This check works on both Linux and OSX
if [[ "$(perl -e '@x=getpwuid($<); print $x[8]')" == "/bin/zsh" ]]; then
    echo "  ${t_green}Shell is already ${t_bold}zsh${t_reset}"
else
    echo "  ${t_yellow}Setting user shell to ${t_bold}zsh${t_reset}${t_yellow}...${t_reset}"
    chsh -s /bin/zsh $(whoami)
fi

# ---------------------------------- #
T   Installing configuration files   #
# ---------------------------------- #

vars_file="$(dirname $0)/vars"
if [ $override_vars -eq 1 ] && [ -f $vars_file ]; then
    rm $vars_file
fi

update_var() {
    human_name=$1
    name=var_$2
    default=${!name}
    echo -n "  ${t_bold}$human_name${t_reset} [$default]: "
    read value
    [[ "$value" == "" ]] && value=${!name}
    echo "$name='$value'" >> $vars_file
}

replace_var() {
    new_fn=$1
    tmp_fn=$(mktemp)
    var_name=$2
    var_val=var_$2
    sed "s/\\\$\\\$${var_name}\\\$\\\$/${!var_val}/g" $new_fn > $tmp_fn
    mv $tmp_fn $new_fn
}

var_git_name="Yoann Bentz"
var_git_email="yoann@yoone.eu"

if [ ! -f $vars_file ]; then
    update_var "Git name" git_name
    update_var "Git email" git_email
    echo
fi

source $vars_file

# Works with files and dirs
cp_conf() {
    src_name=$1
    src=files/$src_name
    dest_dir=$2
    if [ ! -d $dest_dir ]; then
        echo "  ${t_yellow}Creating ${t_bold}${dest_dir}${t_reset}"
        mkdir -p $dest_dir
    fi
    echo "  ${t_green}Copying ${t_bold}${src_name}${t_reset}${t_green} to ${t_bold}$dest_dir${t_reset}"
    if [ -f $src ]; then
        new_fn=$(mktemp)
        cp $src $new_fn
        replace_var $new_fn git_name
        replace_var $new_fn git_email
        mv $new_fn $dest_dir/$src_name
    else
        cp -R $src $dest_dir
    fi
}

cp_conf .oh-my-zsh ~/
cp_conf .aliases ~/
cp_conf .functions ~/
cp_conf .zshrc ~/

cp_conf .vimrc ~/
cp_conf .vim ~/

cp_conf .gitconfig ~/
cp_conf .tmux.conf ~/

echo # Extra line
