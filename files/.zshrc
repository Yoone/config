# Initial PATH
export PATH=/usr/local/bin:$(getconf PATH):~/.local/bin

# Locale
export LANG=en_US.UTF-8

# History
HISTFILE=~/.histfile
HISTSIZE=2000
SAVEHIST=2000

# Reload configuration
trap "source ~/.zshrc" SIGUSR1
reload() {
    source ~/.zshrc
    pkill -SIGUSR1 zsh -u $(id -u $(whoami))
}

# Editor, pager and such
bindkey -e
export VISUAL=vim
export EDITOR=$VISUAL
export PAGER=most
export TERM="screen-256color"

# OSX/Homebrew
if [[ $(uname) == "Darwin" ]]; then
    export PATH=/opt/homebrew/bin:$PATH
    export BINPREFIX="g"
fi

# Ruby virtual env
if [ -d ~/.rvm ]; then
    export PATH=$PATH:~/.rvm/bin
    source ~/.rvm/scripts/rvm
fi

# Rust env
if [ -d ~/.cargo ]; then
    export PATH=$PATH:~/.cargo/bin
fi

# GOPATH
export GOPATH=~/go
export PATH=$PATH:$GOPATH/bin

# Own bin path (scripts to env)
export PATH=$PATH:~/bin

# oh-my-zsh
export ZSH="$HOME/.oh-my-zsh"
ZSH_THEME="yoone" # Needs to be imported from config
plugins=(git)
source $ZSH/oh-my-zsh.sh
setopt no_share_history

# comp
zstyle ':completion:*' completer _complete _ignored _correct _approximate
zstyle ':completion:*' group-name ''
zstyle ':completion:*' insert-unambiguous true
zstyle ':completion:*' list-colors ''
zstyle ':completion:*' list-prompt %SAt %p: Hit TAB for more, or the character to insert%s
zstyle ':completion:*' matcher-list 'm:{[:lower:][:upper:]}={[:upper:][:lower:]}' 'm:{[:lower:][:upper:]}={[:upper:][:lower:]}' 'm:{[:lower:][:upper:]}={[:upper:][:lower:]}' 'm:{[:lower:][:upper:]}={[:upper:][:lower:]}'
zstyle ':completion:*' menu select=1
zstyle ':completion:*' original true
zstyle ':completion:*' select-prompt %SScrolling active: current selection at %p%s
zstyle ':completion:*' use-compctl false
zstyle ':completion:*' verbose true
autoload -Uz compinit
compinit

# Syntax highlighting
zsh_hl_file=~/.oh-my-zsh/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
[ -f $zsh_hl_file ] && source $zsh_hl_file # Dynamic syntax error and hl (strings...) in commands

# kubectl auto-completion
command -v kubectl > /dev/null
if [ $? -eq 0 ]; then
    source <(kubectl completion zsh)
fi

# Include functions, aliases and such
[ -f ~/.functions ] && source ~/.functions
[ -f ~/.aliases ] && source ~/.aliases
[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh # Better ^R search
[ -f ~/.z.sh ] && source ~/.z.sh # Faster navigation between cd'ed dirs
if [ -f ~/.kube-ps1.sh ]; then
    source ~/.kube-ps1.sh # Kubernetes cluster info in prompt
else
    kube_ps1() {}
fi

[ -f ~/.custom_cfg ] && source ~/.custom_cfg # File that does not exist in this repo, allowing to add custom config per machine

# Poetry
export PATH="$HOME/.poetry/bin:$PATH"

# Google Cloud SDK
[ -f "$HOME/google-cloud-sdk/path.zsh.inc" ] && . "$HOME/google-cloud-sdk/path.zsh.inc"
[ -f "$HOME/google-cloud-sdk/completion.zsh.inc" ] && . "$HOME/google-cloud-sdk/completion.zsh.inc"

[ -f "/opt/homebrew/opt/asdf/libexec/asdf.sh" ] && . "/opt/homebrew/opt/asdf/libexec/asdf.sh"

# oxio-cli autocompletion
[ -f "$HOME/.oxio/autocomplete/oxio.zsh" ] && . "$HOME/.oxio/autocomplete/oxio.zsh"
