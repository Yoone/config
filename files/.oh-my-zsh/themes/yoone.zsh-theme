#!/usr/bin/env zsh

ZSH_THEME_GIT_PROMPT_UNTRACKED="%{$fg[red]%}?%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_ADDED="%{$fg[green]%}+%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_DELETED="%{$fg[red]%}x%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_RENAMED="%{$fg[green]%}~%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_MODIFIED="%{$fg[red]%}M%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_STASHED="%{$fg[yellow]%}S%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_UNMERGED="%{$fg[red]%}U%{$reset_color%}"

KUBE_PS1_SYMBOL_ENABLE=false
unset LS_COLORS # Remove cyan for folders

local git_info=

precmd() {
    local git_branch=$(git_current_branch)
    local git_status=$(_omz_git_prompt_status) # Used to be: git_prompt_status

    if [[ "$git_branch" != "" ]]; then
        local git_branch_color=cyan

        if [[ "$git_branch" = master || "$git_branch" = main ]]; then
            git_branch_color=red
        elif [[ "$git_branch" = develop ]]; then
            git_branch_color=green
        fi

        if [[ "$git_status" != "" ]]; then
            git_status=" $git_status"
        fi

        git_info="%{$fg[$git_branch_color]%}‹$git_branch$git_status%{$fg[$git_branch_color]%}›%{$reset_color%} "
    else
        git_info= # Reset when exiting git repo
    fi
}

local current_dir='%{$terminfo[bold]$fg[white]%}%~%{$reset_color%}'
if [[ $UID -eq 0 ]]; then
    local user_symbol="#"
    local user_color=red
else
    local user_symbol="›"
    local user_color=yellow
fi
local user_host="%{$fg[$user_color]%}%n@%m%{$reset_color%}"

PROMPT="$user_host $current_dir \$git_info\$(kube_ps1)
%B%{$fg[$user_color]%}${user_symbol}%{$reset_color%}%b "

local return_code="%(?..%{$fg[red]%}‹%?› %{$reset_color%})"
local date='%{$fg[yellow]%}[%D{%a %b %d, %H:%M}]%{$reset_color%}'
RPS1="%B${return_code}%b${date}"
