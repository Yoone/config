set noexpandtab

setlocal shiftwidth=4
setlocal tabstop=4

" Hiding tabs
set nolist
